import UserActions from "../src/domain/users/userActions"
import UserNicknameUnavailableError from "../src/domain/users/userNicknameUnavailableError";
import User from "../src/domain/users/user"
import UserNotFoundError from "../src/domain/users/userNotFoundError";
import UserService from "../src/domain/users/userService"
import InvalidUserNicknameError from "../src/domain/users/invalidUserNicknameError";
import { when } from "jest-when";


const userServiceRegister = jest.fn();
const userServiceUpdateFullName = jest.fn();
const userServiceAddFollower = jest.fn();
const userServiceGetFollows = jest.fn()
describe('userActions register should', (): void => {
    let fakeUserService: UserService
    let userActions: UserActions
    const NICKNAME: string = 'TEST_USER'
    const FULL_NAME: string = 'Lautaro Sanchez'
    const user: User = new User(NICKNAME, FULL_NAME)

    beforeEach(() => {
        fakeUserService = {} as UserService;
        fakeUserService.register = userServiceRegister;
        userActions = new UserActions(fakeUserService)
    })

    it('throw error through the user service on empty nickname', (): void => {
        userServiceRegister.mockImplementation(() => { throw new InvalidUserNicknameError(NICKNAME) })
        expect(() => userActions.register('', '')).rejects.toThrowError(InvalidUserNicknameError)
    })
    it('return a new user', async (): Promise<void> => {
        userServiceRegister.mockReturnValue(user);
        expect(await userActions.register(NICKNAME, FULL_NAME)).toEqual(user)
    })

    it('register a new user through the user service', () => {
        userActions.register(NICKNAME, FULL_NAME);
        expect(userServiceRegister).toBeCalledWith(NICKNAME, FULL_NAME);
    })
    it('throw Error through the user service on nickname unabailable', (): void => {
        userServiceRegister.mockImplementation(() => { throw new UserNicknameUnavailableError(NICKNAME) })
        expect(() => userActions.register(NICKNAME, FULL_NAME)).rejects.toThrowError(UserNicknameUnavailableError)
    })
})

describe('userActions updateFullName should', (): void => {

    let fakeUserService: UserService
    let userActions: UserActions
    const NICKNAME: string = 'TEST_USER'
    const NEW_FULL_NAME: string = 'Lautaro Sanchez'
    const user: User = new User(NICKNAME, NEW_FULL_NAME)

    beforeEach(() => {
        fakeUserService = {} as UserService;
        fakeUserService.updateFullName = userServiceUpdateFullName;
        userActions = new UserActions(fakeUserService)
    })

    it('return the new full name', async (): Promise<void> => {
        userServiceUpdateFullName.mockReturnValue(NEW_FULL_NAME)
        expect(await userActions.updateFullName(NEW_FULL_NAME, NICKNAME)).toBe(NEW_FULL_NAME)
    })
    it('throw error through user service when the nickname is not of a user', (): void => {
        userServiceUpdateFullName.mockImplementation(() => { throw new UserNotFoundError(NICKNAME) })
        expect(() => userActions.updateFullName(NEW_FULL_NAME, NICKNAME)).rejects.toThrowError(UserNotFoundError)
    })
})
describe('userActions addFollower should', (): void => {

    let fakeUserService: UserService
    let userActions: UserActions
    const FOLLOWER_NICKNAME: string = 'FOLOWER_USER'
    const FOLLOWED_NICKNAME: string = 'FOLOWED_USER'

    beforeEach(() => {
        fakeUserService = {} as UserService;
        fakeUserService.addFollower = userServiceAddFollower;
        userActions = new UserActions(fakeUserService)
    })

    it('add a follower through the user service', async (): Promise<void> => {
        await userActions.addFollower(FOLLOWER_NICKNAME, FOLLOWED_NICKNAME)
        expect(userServiceAddFollower).toBeCalledWith(FOLLOWER_NICKNAME, FOLLOWED_NICKNAME)
    })
    it('throw error through user service when the nickname is not of a user', (): void => {
        userServiceAddFollower.mockImplementation(() => { throw new UserNotFoundError('') })
        expect(() => userActions.addFollower('', '')).rejects.toThrowError(UserNotFoundError)
    })
})

describe('userActions getFollows should', (): void => {

    let fakeUserService: UserService
    let userActions: UserActions
    const NICKNAME: string = 'TEST_USER'
    const TO_FOLLOW_NICKNAME: string = 'TO_FOLLOW_NICKNAME'
    const FULL_NAME: string = 'Lautaro Sanchez'
    const user: User = new User(NICKNAME, FULL_NAME)

    beforeEach(() => {
        fakeUserService = {} as UserService;
        fakeUserService.getFollows = userServiceGetFollows;
        userActions = new UserActions(fakeUserService)
    })
    it('get follows through the user service', async (): Promise<void> => {

        when(userServiceGetFollows).calledWith(NICKNAME).mockReturnValue(new Set<string>([TO_FOLLOW_NICKNAME]))
        expect(await userActions.getFollows(NICKNAME)).toEqual(new Set<string>([TO_FOLLOW_NICKNAME]))
    })
    it('throw error through user service when the nickname is not of a user', (): void => {
        userServiceGetFollows.mockImplementation(() => { throw new UserNotFoundError('') })
        expect(() => userActions.getFollows(NICKNAME)).rejects.toThrowError(UserNotFoundError)
    })
})

describe('userActions tweet should',() =>{
    const TWEET:string='im tweeting this'
    const NICKNAME: string = 'NICKNAME'
    let fakeUserService:UserService
    let userActions:UserActions
    beforeEach(()=>{
        fakeUserService = {} as UserService
        userActions= new UserActions(fakeUserService)
    })
    it('save a tweet through userService',async ()=>{
        const tweet = jest.fn()
        fakeUserService.tweet = tweet
        await userActions.tweet(NICKNAME,TWEET)
        expect(tweet).toBeCalledWith(NICKNAME,TWEET)
    })
})

describe('userActions getTweets should',() =>{
    const TWEET:string='im tweeting this'
    const NICKNAME: string = 'NICKNAME'
    let fakeUserService:UserService
    let userActions:UserActions
    beforeEach(()=>{
        fakeUserService = {} as UserService
        userActions= new UserActions(fakeUserService)
    })
    it('get tweets from a user through userService',async ()=>{
        const getTweets = jest.fn()
        fakeUserService.getTweets = getTweets
        when(getTweets).calledWith(NICKNAME).mockResolvedValueOnce([TWEET])
        expect(await userActions.getTweets(NICKNAME)).toEqual([TWEET])
    })
})
