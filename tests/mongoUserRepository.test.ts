import User from "../src/domain/users/user"
import UserRepository from "../src/domain/users/userRepository"
import UserNotFoundError from "../src/domain/users/userNotFoundError"
import MongoUserRepository, { UserData } from "../src/infrastructure/repositories/mongoUserRepository"
import { Db, MongoClient, Collection } from "mongodb"
import { when } from "jest-when"
import mongoose from 'mongoose'

class FakeMongoUserRepository extends MongoUserRepository {
    constructor() {
        super()
    }
    public model = super.model
    public getBuiltModel = super.getBuiltModel
}

describe('mongoUserRepository findByNickname should', (): void => {
    let userRepository: FakeMongoUserRepository

    const NICKNAME: string = 'NICKNAME'
    const FOLLOWED_NICKNAME: string = 'FOLLOWED_NICKNAME'
    const TWEET: string = 'TWEET'
    const FULL_NAME: string = 'FULL_NAME'
    const userData: UserData = {
        nickname: NICKNAME,
        fullName: FULL_NAME,
        folledUsers: [FOLLOWED_NICKNAME],
        tweets: [TWEET]
    }
    let findOne = jest.fn()
    beforeEach(() => {
        userRepository = new FakeMongoUserRepository()
        userRepository.model = {} as mongoose.Model<mongoose.Document<any>>
        findOne = jest.fn()
        userRepository.model.findOne = findOne

    })
    it('finds the data of the user with the nickname', async (): Promise<void> => {

        findOne.mockReturnValueOnce(Promise.resolve(userData))
        await userRepository.findByNickname(NICKNAME)
        expect(findOne).toBeCalled()
        expect(findOne).toBeCalledWith({ nickname: NICKNAME })
    })
    it('returns the user with the nickname', async (): Promise<void> => {
        const user = new User(userData.nickname, userData.fullName)
        user.addFollow(FOLLOWED_NICKNAME)
        user.addTweet(TWEET)
        const findOne = jest.fn()
        userRepository.model.findOne = findOne
        when(findOne).calledWith({ nickname: NICKNAME }).mockReturnValueOnce(Promise.resolve(userData))
        expect(await userRepository.findByNickname(NICKNAME)).toEqual(user)
    })
    it('returns null if user is not found', async (): Promise<void> => {
        const findOne = jest.fn()
        userRepository.model.findOne = findOne
        findOne.mockReturnValueOnce(Promise.resolve(null))
        expect(await userRepository.findByNickname(NICKNAME)).toBeNull()
    })
})

describe('mongoUserRepository isNicknameAvailable should', (): void => {
    let userRepository: FakeMongoUserRepository

    const NICKNAME: string = 'NICKNAME'
    const FOLLOWED_NICKNAME: string = 'FOLLOWED_NICKNAME'
    let findByNickname = jest.fn()
    beforeEach(() => {
        userRepository = new FakeMongoUserRepository()
        userRepository.model = {} as mongoose.Model<mongoose.Document<any>>
        findByNickname = jest.fn()
        userRepository.findByNickname = findByNickname

    })
    it('return true if a user is not found', async (): Promise<void> => {
        findByNickname.mockReturnValueOnce(Promise.resolve(null))
        expect(await userRepository.isNicknameAvailable(NICKNAME)).toBe(true)
    })
    it('return false if a user is found', async (): Promise<void> => {
        const user = new User('', '')
        findByNickname.mockReturnValueOnce(Promise.resolve(user))
        expect(await userRepository.isNicknameAvailable(NICKNAME)).toBe(false)
    })
})

describe('mongoUserRepository addNewUser should', (): void => {
    let userRepository: FakeMongoUserRepository

    const NICKNAME: string = 'NICKNAME'
    const FULL_NAME: string = 'FULL_NAME'
    const user = new User(NICKNAME, FULL_NAME)
    let getBuiltModel = jest.fn()
    beforeEach(() => {
        userRepository = new FakeMongoUserRepository()
        userRepository.getBuiltModel = getBuiltModel
    })
    it('return the new user', async (): Promise<void> => {
        const newUser = {} as mongoose.Document<any>
        newUser.save = jest.fn()
        getBuiltModel.mockReturnValueOnce(newUser)
        expect(await userRepository.addNewUser(NICKNAME, FULL_NAME)).toEqual(user)
    })
})

describe('mongoUserRepository getUsers should', (): void => {
    let userRepository: FakeMongoUserRepository

    const NICKNAME: string = 'NICKNAME'
    const FOLLOWED_NICKNAME: string = 'FOLLOWED_NICKNAME'
    const FULL_NAME: string = 'FULL_NAME'
    const TWEET:string = 'TWEET'
    const userData: UserData = {
        nickname: NICKNAME,
        fullName: FULL_NAME,
        folledUsers: [FOLLOWED_NICKNAME],
        tweets: [TWEET]
    }
    const user = new User(NICKNAME, FULL_NAME)
    user.addFollow(FOLLOWED_NICKNAME)
    user.addTweet(TWEET)
    beforeEach(() => {
        userRepository = new FakeMongoUserRepository()
        userRepository.model = {} as mongoose.Model<mongoose.Document<any>>
    })
    it('finds the data of all the users', async (): Promise<void> => {
        const find = jest.fn()
        userRepository.model.find = find
        find.mockReturnValueOnce(Promise.resolve([userData]))
        await userRepository.getUsers()
        expect(find).toBeCalledWith({})
    })
    it('returns all the users', async (): Promise<void> => {
        const find = jest.fn()
        userRepository.model.find = find
        find.mockReturnValueOnce(Promise.resolve([userData]))
        expect(await userRepository.getUsers()).toEqual([user])
    })
})

describe('mongoUserRepository updateUser should', (): void => {
    let userRepository: FakeMongoUserRepository

    const NICKNAME: string = 'NICKNAME'
    const FOLLOWED_NICKNAME: string = 'FOLLOWED_NICKNAME'
    const FULL_NAME: string = 'FULL_NAME'
    const TWEET:string = 'TWEET'
    const user = new User(NICKNAME, FULL_NAME)
    user.addFollow(FOLLOWED_NICKNAME)
    user.addTweet(TWEET)
    const userData: UserData = {
        nickname: NICKNAME,
        fullName: FULL_NAME,
        folledUsers: [FOLLOWED_NICKNAME],
        tweets:[TWEET]
    }
    beforeEach(() => {
        userRepository = new FakeMongoUserRepository()
        userRepository.model = {} as mongoose.Model<mongoose.Document<any>>
    })
    it('return the updated user', async (): Promise<void> => {
        const update = jest.fn()
        userRepository.model.update = update
        expect(await userRepository.updateUser(user)).toEqual(user)
    })
    it('persist the updated user', async (): Promise<void> => {
        const update = jest.fn()
        userRepository.model.update = update
        await userRepository.updateUser(user)
        expect(update).toBeCalledWith({ nickname: NICKNAME }, { ...userData })
    })
})