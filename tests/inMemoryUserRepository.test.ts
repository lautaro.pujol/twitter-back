import User from "../src/domain/users/user"
import InMemoryUserRepository, { InMemoryUser } from "../src/infrastructure/repositories/inMemoryUserRepository"
import UserRepository from "../src/domain/users/userRepository"
import UserNotFoundError from "../src/domain/users/userNotFoundError"

describe('inMemoryRepository addNewUser should', (): void => {
    let userRepository: UserRepository
    const NICKNAME: string = 'TEST_USER'
    const FULL_NAME: string = 'Lautaro Sanchez'
    const user: User = new User(NICKNAME, FULL_NAME)

    beforeEach(() => {
        userRepository = new InMemoryUserRepository()
    })
    it('return new user', async (): Promise<void> => {
        expect(await userRepository.addNewUser(NICKNAME, FULL_NAME)).toEqual(user)
    })
    it('save new user', async (): Promise<void> => {
        const user = await userRepository.addNewUser(NICKNAME, FULL_NAME)
        expect(await userRepository.getUsers()).toEqual([user])
    })
})

describe('inMemoryRepository nicknameIsAvailable should', (): void => {
    let userRepository: UserRepository
    const NICKNAME: string = 'TEST_USER'
    const FULL_NAME: string = 'Lautaro Sanchez'

    beforeEach(() => {
        userRepository = new InMemoryUserRepository()
    })
    it('return true if no user has the nickname', async (): Promise<void> => {
        expect(await userRepository.isNicknameAvailable(NICKNAME)).toBe(true)
    })
    it('return false a user has the nickname', async (): Promise<void> => {
        await userRepository.addNewUser(NICKNAME, FULL_NAME)
        expect(await userRepository.isNicknameAvailable(NICKNAME)).toBe(false)
    })
})

describe('inMemoryRepository updateUser should', (): void => {
    let userRepository: UserRepository
    const NICKNAME: string = 'TEST_USER'
    const NICKNAME_FOLLOWD: string = 'FOLLOWD_USER'
    const NEW_FULL_NAME: string = 'nuevonombre martinez'
    const FULL_NAME: string = 'Leandro Sanchez'
    let updatedUser: User

    beforeEach(() => {
        userRepository = new InMemoryUserRepository()
        updatedUser = new User(NICKNAME, NEW_FULL_NAME)
    })
    it('return the updated user', async (): Promise<void> => {
        await userRepository.addNewUser(NICKNAME, FULL_NAME)
        expect(await userRepository.updateUser(updatedUser)).toEqual(updatedUser)
    })
    it('throw error when user with the nickname not found', (): void => {
        expect(() => userRepository.updateUser(updatedUser)).toThrowError(UserNotFoundError)
    })
    it('update user fullname', async (): Promise<void> => {
        await userRepository.addNewUser(NICKNAME, FULL_NAME)
        userRepository.updateUser(updatedUser)
        expect(await userRepository.getUsers()).toEqual([updatedUser])
    })
    it('update user follows', async (): Promise<void> => {
        const user = await userRepository.addNewUser(NICKNAME, FULL_NAME)
        user.addFollow(NICKNAME_FOLLOWD)
        userRepository.updateUser(user)
        expect(await userRepository.findByNickname(NICKNAME)).toEqual(user)
    })
})


describe('inMemoryRepository findByNickname should', (): void => {
    let userRepository: UserRepository
    const NICKNAME: string = 'TEST_USER'
    const FULL_NAME: string = 'Lautaro Sanchez'
    beforeEach(() => {
        userRepository = new InMemoryUserRepository()
    })
    it('return the user found', async (): Promise<void> => {
        const user = await userRepository.addNewUser(NICKNAME, FULL_NAME)
        expect(await userRepository.findByNickname(NICKNAME)).toEqual(user)
    })
    it('return null when no user has the nickname', async (): Promise<void> => {
        expect(await userRepository.findByNickname(NICKNAME)).toBeNull()
    })
})