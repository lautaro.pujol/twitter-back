import User from "../src/domain/users/user"
import UserService from "../src/domain/users/userService"
import UserRepository from "../src/domain/users/userRepository"
import NicknameUnavailableError from "../src/domain/users/userNicknameUnavailableError"
import UserNotFoundError from "../src/domain/users/userNotFoundError"
import InvalidUserNicknameError from "../src/domain/users/invalidUserNicknameError"
import { when } from "jest-when"
const userRepositoryAddNewUser = jest.fn();
const userRepositoryNicknameIsAvailable = jest.fn();
const userRepositoryUpdateUser = jest.fn();
const userRepositoryFindByNickname = jest.fn();

describe('userService register should', (): void => {
    let userService: UserService
    let fakeUserRepository: UserRepository

    const NICKNAME: string = 'TEST_USER'
    const FULL_NAME: string = 'Lautaro Sanchez'
    const user: User = new User(NICKNAME, FULL_NAME)

    beforeEach(() => {
        fakeUserRepository = {} as UserRepository
        fakeUserRepository.addNewUser = userRepositoryAddNewUser;
        fakeUserRepository.isNicknameAvailable = userRepositoryNicknameIsAvailable;
        userService = new UserService(fakeUserRepository)
    })


    it('throw Error when nickname is empty', (): void => {
        expect(() => userService.register('', '')).rejects.toThrowError(InvalidUserNicknameError)
    })
    it('add a new user through the user repository', async (): Promise<void> => {
        userRepositoryNicknameIsAvailable.mockReturnValue(true)
        await userService.register(NICKNAME, FULL_NAME)
        expect(userRepositoryAddNewUser).toBeCalledWith(NICKNAME, FULL_NAME)
    })
    it('return a new user', (): void => {
        const user = userService.register(NICKNAME, FULL_NAME)
        userRepositoryAddNewUser.mockReturnValue(user)
        expect(userService.register(NICKNAME, FULL_NAME)).toEqual(user)
    })
    it('throw Error when nickname is unabilable', (): void => {
        userRepositoryNicknameIsAvailable.mockReturnValue(false)
        expect(() => userService.register(NICKNAME, FULL_NAME)).rejects.toThrowError(NicknameUnavailableError)
    })
})

describe('userService updateFullName should', (): void => {
    let userService: UserService
    let fakeUserRepository: UserRepository

    const NICKNAME: string = 'TEST_USER'
    const NEW_FULL_NAME: string = 'Lautaro Sanchez'
    const user: User = new User(NICKNAME, '')
    const updatedUser: User = new User(NICKNAME, NEW_FULL_NAME)

    beforeEach(() => {
        fakeUserRepository = {} as UserRepository
        fakeUserRepository.updateUser = userRepositoryUpdateUser;
        fakeUserRepository.findByNickname = userRepositoryFindByNickname;
        userService = new UserService(fakeUserRepository)
    })
    it('throw error when the nickname is not of a user', (): void => {
        userRepositoryFindByNickname.mockReturnValue(null)
        expect(() => userService.updateFullName(NEW_FULL_NAME, NICKNAME)).rejects.toThrowError(UserNotFoundError)
    })
    it('return the new fullname', async (): Promise<void> => {
        userRepositoryFindByNickname.mockReturnValue(user)
        userRepositoryUpdateUser.mockReturnValue(updatedUser)
        expect(await userService.updateFullName(NEW_FULL_NAME, NICKNAME)).toBe(NEW_FULL_NAME)
    })
})

describe('userService addFollower should', (): void => {

    let userService: UserService
    let fakeUserRepository: UserRepository
    let followerUser: User
    let userToFollow: User

    const FOLLOWER_NICKNAME: string = 'FOLLOWER_NICKNAME'
    const NICKNAME_TO_FOLLOW: string = 'USER_TO_FOLLOW'


    beforeEach(() => {
        fakeUserRepository = {} as UserRepository
        fakeUserRepository.updateUser = userRepositoryUpdateUser;
        fakeUserRepository.findByNickname = userRepositoryFindByNickname;
        followerUser = new User(FOLLOWER_NICKNAME, 'Follower')
        userToFollow = new User(NICKNAME_TO_FOLLOW, 'toFollow')
        userService = new UserService(fakeUserRepository)
    })

    it('search users through userRepository', async (): Promise<void> => {
        userRepositoryFindByNickname.mockReturnValue(followerUser)
        await userService.addFollower(FOLLOWER_NICKNAME, NICKNAME_TO_FOLLOW)
        expect(userRepositoryFindByNickname).toBeCalledWith(FOLLOWER_NICKNAME)
        expect(userRepositoryFindByNickname).toBeCalledWith(NICKNAME_TO_FOLLOW)
    })
    it('throw error when the follower searched is not of a user', (): void => {
        when(userRepositoryFindByNickname).calledWith(FOLLOWER_NICKNAME).mockReturnValue(null)
        when(userRepositoryFindByNickname).calledWith(NICKNAME_TO_FOLLOW).mockReturnValue(userToFollow)
        expect(() => userService.addFollower(FOLLOWER_NICKNAME, NICKNAME_TO_FOLLOW)).rejects.toThrowError(new UserNotFoundError(FOLLOWER_NICKNAME))
    })
    it('throw error when the nickname to follow is not of a user', (): void => {
        when(userRepositoryFindByNickname).calledWith(FOLLOWER_NICKNAME).mockReturnValue(followerUser)
        when(userRepositoryFindByNickname).calledWith(NICKNAME_TO_FOLLOW).mockReturnValue(null)
        expect(() => userService.addFollower(FOLLOWER_NICKNAME, NICKNAME_TO_FOLLOW)).rejects.toThrowError(new UserNotFoundError(NICKNAME_TO_FOLLOW))
    })
    it('add followed through userRepository', async (): Promise<void> => {
        const updatedUser: User = followerUser
        updatedUser.addFollow(NICKNAME_TO_FOLLOW)
        when(userRepositoryFindByNickname).calledWith(FOLLOWER_NICKNAME).mockReturnValue(followerUser)
        when(userRepositoryFindByNickname).calledWith(NICKNAME_TO_FOLLOW).mockReturnValue(userToFollow)
        await userService.addFollower(FOLLOWER_NICKNAME, NICKNAME_TO_FOLLOW)
        expect(userRepositoryUpdateUser).toBeCalledWith(updatedUser)
    })
})

describe('userService getFollows should', (): void => {
    let userService: UserService
    let fakeUserRepository: UserRepository
    let user: User

    const FOLLOWER_NICKNAME: string = 'FOLLOWER_NICKNAME'
    const NICKNAME_TO_FOLLOW: string = 'USER_TO_FOLLOW'


    beforeEach(() => {
        user = new User(FOLLOWER_NICKNAME, 'name')
        user.addFollow(NICKNAME_TO_FOLLOW)
        fakeUserRepository = {} as UserRepository
        fakeUserRepository.findByNickname = userRepositoryFindByNickname
        userService = new UserService(fakeUserRepository)
    })
    it('return a set with the nickname of followed users', async (): Promise<void> => {
        userRepositoryFindByNickname.mockReturnValue(user)
        expect(await userService.getFollows(FOLLOWER_NICKNAME)).toEqual(new Set<string>([NICKNAME_TO_FOLLOW]))
    })
    it('throw error through userRepository when the nickname to follow is not of a user', (): void => {
        userRepositoryFindByNickname.mockReturnValue(null)
        expect(() => userService.getFollows(FOLLOWER_NICKNAME)).rejects.toThrowError(new UserNotFoundError(FOLLOWER_NICKNAME))
    })
})

describe('userService tweet should', () => {
    let fakeUserRepository: UserRepository
    let userService: UserService
    const NICKNAME = 'NICKNAME'
    const TWEET = 'TWEET'
    const FULL_NAME = 'FULL_NAME'
    const user = new User(NICKNAME, FULL_NAME)
    beforeEach(() => {
        fakeUserRepository = {} as UserRepository
        userService = new UserService(fakeUserRepository)
    })
    it('update user with a new tweet thorugh userRepository', async (): Promise<void> => {
        const updateUser = jest.fn()
        fakeUserRepository.updateUser = updateUser
        const findByNickname = jest.fn()
        fakeUserRepository.findByNickname = findByNickname
        findByNickname.mockResolvedValueOnce(user)
        const updatedUser = new User(NICKNAME, FULL_NAME)
        updatedUser.addTweet(TWEET)
        await userService.tweet(NICKNAME, TWEET)
        expect(updateUser).toBeCalledWith(updatedUser)
    })
})

describe('userService getTweets should', () => {
    let fakeUserRepository: UserRepository
    let userService: UserService
    const NICKNAME = 'NICKNAME'
    const TWEET = 'TWEET'
    const FULL_NAME = 'FULL_NAME'
    const user = new User(NICKNAME, FULL_NAME)
    user.addTweet(TWEET)
    beforeEach(() => {
        fakeUserRepository = {} as UserRepository
        userService = new UserService(fakeUserRepository)
    })
    it('return the tweets of the user thorugh userRepository', async (): Promise<void> => {
        const findByNickname = jest.fn()
        fakeUserRepository.findByNickname = findByNickname
        when(findByNickname).calledWith(NICKNAME).mockResolvedValueOnce(user)
        expect(await userService.getTweets(NICKNAME)).toEqual([TWEET])
    })
})