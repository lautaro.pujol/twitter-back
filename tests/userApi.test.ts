import { request, Request, Response } from "express"
import { when } from "jest-when"
import User from '../src/domain/users/user'
import UserActions from "../src/domain/users/userActions"
import UserApi, { RegisterData, FindOneByNicknameData, UpdateFullNameData, AddFollowData, GetFollowedData, TweetBodyData, GetTweetBodyData } from "../src/infrastructure/api/userApi"

const NICKNAME: string = 'TEST_USER'
const FULL_NAME: string = 'Lautaro Sanchez'
const testUser = new User(NICKNAME, FULL_NAME)

describe('userApi register should', (): void => {

    const fakeUserActions = {} as UserActions
    const register = jest.fn()
    fakeUserActions.register = register

    let send = jest.fn()
    let status = jest.fn()
    const userApi: UserApi = new UserApi(fakeUserActions)


    let request: Request
    let response: Response
    const registerData: RegisterData = {
        fullName: FULL_NAME,
        nickname: NICKNAME
    }


    beforeEach(() => {
        send = jest.fn()
        status = jest.fn()
        request = {} as Request
        response = {} as Response

        response.send = send
        response.status = status
        status.mockReturnValue(response)
    })

    it('register a new user with userService', async (): Promise<void> => {
        request.body = registerData
        register.mockReturnValueOnce(testUser)

        await userApi.register(request, response)
        expect(register).toBeCalledWith(registerData.nickname, registerData.fullName)
    })
    it('send a json representing the new user', async (): Promise<void> => {
        request.body = registerData
        register.mockReturnValueOnce(testUser)
        await userApi.register(request, response)
        expect(status).toBeCalledWith(200)
        expect(send).toBeCalledWith(testUser)
    })
})

describe('userApi updateFullName should', (): void => {

    const fakeUserActions = {} as UserActions
    const updateFullName = jest.fn()
    fakeUserActions.updateFullName = updateFullName
    const userApi: UserApi = new UserApi(fakeUserActions)
    let request: Request
    let response: Response

    let send = jest.fn()
    let status = jest.fn()
    let NEW_FULL_NAME = 'Andrea Stevan'

    const updateFullNameData: UpdateFullNameData = {
        fullName: NEW_FULL_NAME,
        nickname: NICKNAME
    }

    beforeEach(() => {
        send = jest.fn()
        status = jest.fn()
        request = {} as Request
        response = {} as Response

        response.send = send
        response.status = status
        status.mockReturnValue(response)
    })

    it('update the fullName of a user with userService', async (): Promise<void> => {
        request.body = updateFullNameData
        updateFullName.mockReturnValueOnce(NEW_FULL_NAME)

        await userApi.updateFullName(request, response)
        expect(updateFullName).toBeCalledWith(updateFullNameData.fullName, updateFullNameData.nickname)
    })
    it('send the new fullName', async (): Promise<void> => {
        request.body = updateFullNameData
        updateFullName.mockReturnValueOnce(NEW_FULL_NAME)
        await userApi.updateFullName(request, response)
        expect(status).toBeCalledWith(200)
        expect(send).toBeCalledWith(NEW_FULL_NAME)
    })
})


describe('userApi addFollower should', (): void => {

    const fakeUserActions = {} as UserActions
    const addFollower = jest.fn()
    fakeUserActions.addFollower = addFollower
    const userApi: UserApi = new UserApi(fakeUserActions)
    let request: Request
    let response: Response

    let send = jest.fn()
    let status = jest.fn()
    const NICKNAME_TO_FOLLOW: string = 'NICKNAME_TO_FOLLOW'
    const addFollowerData: AddFollowData = {
        followerNickname: NICKNAME,
        nicknameToFollow: NICKNAME_TO_FOLLOW
    }
    beforeEach(() => {
        send = jest.fn()
        status = jest.fn()
        request = {} as Request
        response = {} as Response

        response.send = send
        response.status = status
        status.mockReturnValue(response)
    })

    it('set a user to follow another with userService', async (): Promise<void> => {
        request.body = addFollowerData

        await userApi.addFollower(request, response)
        expect(addFollower).toBeCalledWith(NICKNAME, NICKNAME_TO_FOLLOW)
    })
    it('send 204 response', async (): Promise<void> => {
        request.body = addFollowerData
        await userApi.addFollower(request, response)
        expect(status).toBeCalledWith(204)
        expect(send).toBeCalled()
    })
})

describe('userAPI getFollows should', (): void => {

    const fakeUserActions = {} as UserActions
    const getFollows = jest.fn()
    fakeUserActions.getFollows = getFollows
    const userApi: UserApi = new UserApi(fakeUserActions)
    let request: Request
    let response: Response

    let send = jest.fn()
    let status = jest.fn()
    let NICKNAME_TO_FOLLOW: string = 'NICKNAME_TO_FOLLOW'
    let getFollowedData: GetFollowedData = {
        nickname: NICKNAME,
    }
    beforeEach(() => {
        send = jest.fn()
        status = jest.fn()
        request = {} as Request
        response = {} as Response
        request.body = getFollowedData

        response.send = send
        response.status = status
        status.mockReturnValue(response)
    })

    it('get the followed users form userService', async (): Promise<void> => {
        getFollows.mockRejectedValueOnce(new Set())
        userApi.getFollowedUsers(request, response).then(() => expect(getFollows).toBeCalledWith(NICKNAME))
    })

    it('send json array with the nickname of the followed users', async (): Promise<void> => {
        getFollows.mockReturnValueOnce(new Set([NICKNAME_TO_FOLLOW]))

        await userApi.getFollowedUsers(request, response)
        expect(status).toBeCalledWith(200)
        expect(send).toBeCalledWith([NICKNAME_TO_FOLLOW])
    })
})

describe('userAPI tweet should', (): void => {
    let userApi: UserApi
    const fakeUserActions = {} as UserActions
    const TWEET: string = 'I tweeted this'
    const tweetBodyData: TweetBodyData = {
        nickname: NICKNAME,
        tweet: TWEET
    }
    let request = {} as Request
    let response = {} as Response
    let status = jest.fn()
    const send = jest.fn()

    beforeEach(() => {
        userApi = new UserApi(fakeUserActions)
        request = {} as Request
        response = {} as Response
        status = jest.fn()
        response.status = status
        status.mockReturnValueOnce(response)
        response.send = send
    })
    it('post a new tweet through userActions', async () => {
        const tweet = jest.fn()
        fakeUserActions.tweet = tweet
        request.body = tweetBodyData
        await userApi.tweet(request, response)
        expect(tweet).toBeCalledWith(NICKNAME, TWEET)
    })
    it('send a status 204 response', async () => {
        const tweet = jest.fn()
        tweet.mockResolvedValueOnce(null)
        fakeUserActions.tweet = tweet
        request.body = tweetBodyData

     
        await userApi.tweet(request, response)
        expect(status).toBeCalledWith(204)
        expect(send).toBeCalled()
    })
})

describe('userAPI getTweets should', (): void => {
    let userApi: UserApi
    const fakeUserActions = {} as UserActions
    const TWEET: string = 'I tweeted this'
    const tweetBodyData: GetTweetBodyData = {
        nickname: NICKNAME,
    }
    let request = {} as Request
    let response = {} as Response
    let status = jest.fn()
    const send = jest.fn()

    beforeEach(() => {
        userApi = new UserApi(fakeUserActions)
        request = {} as Request
        response = {} as Response
        status = jest.fn()
        response.status = status
        status.mockReturnValueOnce(response)
        response.send = send
    })
    it('get the users tweets through ', async () => {
        const getTweets = jest.fn()
        fakeUserActions.getTweets = getTweets
        request.query = tweetBodyData as any
        await userApi.getTweets(request, response)
        expect(getTweets).toBeCalledWith(NICKNAME)
    })
    it('send tweets as response', async () => {
        const tweets =[TWEET] 
        const tweet = jest.fn()
        tweet.mockResolvedValueOnce(null)
        const getTweets = jest.fn()
        fakeUserActions.getTweets = getTweets
        request.query = tweetBodyData as any

        when(getTweets).calledWith(NICKNAME).mockResolvedValueOnce([TWEET])

        await userApi.getTweets(request, response)
        expect(status).toBeCalledWith(200)
        expect(send).toBeCalledWith(tweets)
    })
})