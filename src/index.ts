import UserActions from "./domain/users/userActions"
import UserService from "./domain/users/userService"
import MongoUserRepository from "./infrastructure/repositories/mongoUserRepository"
import UserApi from "./infrastructure/api/userApi"
import express from 'express';
import cors from 'cors';

const app = express()
app.use(express.json())
app.use(cors())
const port = 8000




const userApi = new UserApi(new UserActions(new UserService(new MongoUserRepository)))

app.get('/user/getFollowedUsers', (req, res) => userApi.getFollowedUsers(req, res))
app.post('/user/register', (req, res) => userApi.register(req, res))
app.post('/user/updateFullName', (req, res) => userApi.updateFullName(req, res))
app.post('/user/addFollower', (req, res) => userApi.addFollower(req, res))
app.post('/user/tweet', (req, res) => userApi.tweet(req, res))
app.get('/user/getTweets', (req, res) => userApi.getTweets(req, res))
app.get('/user/login', (req, res) => userApi.login(req, res))

const server = app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`)
})