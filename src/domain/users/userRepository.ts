import User from "../../domain/users/user";

export default interface UserRepository {
    findByNickname(nickname:string):Promise<User>;
    updateUser(user:User): Promise<User>;
    isNicknameAvailable(nickname: string): Promise<boolean>;
    getUsers(): Promise<Array<User>>
    addNewUser(nickname: string, fullName: string): Promise<User>
}