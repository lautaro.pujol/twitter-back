import User from "./user"
import UserService from "./userService"


export default class UserActions {
    async login(nickname: string) {
        return await this.userService.login(nickname)
    }
    async getTweets(nickname: string): Promise<string[]> {
        return await this.userService.getTweets(nickname)
    }
    async tweet(nickname: string, tweet: string): Promise<void> {
        await this.userService.tweet(nickname, tweet)
    }
    async getFollows(NICKNAME: string): Promise<Set<string>> {
        return await this.userService.getFollows(NICKNAME)
    }
    async addFollower(followerNickname: string, followedNickname: string) {
        return await this.userService.addFollower(followerNickname, followedNickname)
    }
    async updateFullName(NEW_FULL_NAME: string, NICKNAME: string): Promise<string> {
        return await this.userService.updateFullName(NEW_FULL_NAME, NICKNAME)
    }

    private userService: UserService
    constructor(userService: UserService) {
        this.userService = userService
    }
    async register(nickname: string, fullName: string): Promise<User> {
        return await this.userService.register(nickname, fullName)
    }
}