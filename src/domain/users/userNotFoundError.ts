export default class UserNotFoundError extends Error {
    constructor(nickname:string) {
        super(`User '${nickname}' was not found`)
    }
}