export default class User {
    setTweets(tweets: string[]) {
        this.tweets = tweets
    }
    getTweets() {
        return this.tweets
    }
    addTweet(tweet: string) {
        this.tweets.push(tweet)
    }

    protected tweets: string[] = [];
    protected nickname: string;
    protected fullName: string;
    protected folledUsers: Set<string> = new Set()
    constructor(nickname: string, fullName: string) {
        this.nickname = nickname
        this.fullName = fullName
    }
    updateFullName(newFullName: string) {
        this.fullName = newFullName
    }
    setFollowedUsers(folledUsers: Set<string>) {
        this.folledUsers = folledUsers
    }
    public getNickname(): string {
        return this.nickname
    }
    public getFullName(): string {
        return this.fullName
    }
    getFollowedUsers(): Set<string> {
        return this.folledUsers
    }
    addFollow(nicknameToFollow: string) {
        this.folledUsers.add(nicknameToFollow)
    }
}