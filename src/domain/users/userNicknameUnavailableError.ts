export default class NicknameUnavailableError extends Error {
    constructor(nickname:string) {
        super(`Nickname '${nickname}' already in use`)
    }
}