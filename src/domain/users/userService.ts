import NicknameUnavailableError from "./userNicknameUnavailableError";
import User from "./user";
import UserRepository from "./userRepository";
import InvalidUserNicknameError from "./invalidUserNicknameError";
import UserNotFoundError from "./userNotFoundError";

export default class UserService {
    async login(nickname: string) {
        return await this.findByNickname(nickname)
    }
    async getTweets(nickname: string): Promise<string[]> {
        const user = await this.findByNickname(nickname)
        return user.getTweets()
    }
    async tweet(nickname: string,tweet:string): Promise<void> {
        const user = await this.findByNickname(nickname)
        user.addTweet(tweet)
        this.userRepository.updateUser(user)
    }
    async getFollows(nickname: string): Promise<Set<string>> {
        const user = await this.findByNickname(nickname)
        return user.getFollowedUsers()
    }
    private userRepository: UserRepository

    constructor(userRepository: UserRepository) {
        this.userRepository = userRepository
    }

    async addFollower(followerNickname: string, nicknameToFollow: string): Promise<void> {
        const userToFollowPromise = this.findByNickname(nicknameToFollow)
        const followerUserPromise = this.findByNickname(followerNickname)
        const userToFollow = await userToFollowPromise
        const followerUser = await followerUserPromise
        followerUser.addFollow(userToFollow.getNickname())
        await this.userRepository.updateUser(followerUser)
    }
    async updateFullName(newFullName: string, nickname: string): Promise<string> {
        const user = await this.findByNickname(nickname);
        user.updateFullName(newFullName)
        return (await this.userRepository.updateUser(user)).getFullName()
    }
    private async findByNickname(nickname: string) {
        const user = await this.userRepository.findByNickname(nickname);
        if (!user)
            throw new UserNotFoundError(nickname);
        return user;
    }

    async getUsers(): Promise<Array<User>> {
        return await this.userRepository.getUsers()
    }

    async register(nickname: string, fullName: string): Promise<User> {
        if (!this.nicknameIsValid(nickname))
            throw new InvalidUserNicknameError(nickname)
        if (!await this.userRepository.isNicknameAvailable(nickname))
            throw new NicknameUnavailableError(nickname)
        return await this.userRepository.addNewUser(nickname, fullName)
    }
    private nicknameIsValid(nickname: string) {
        return nickname.length !== 0
    }
}