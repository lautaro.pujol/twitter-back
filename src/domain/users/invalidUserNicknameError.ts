export default class InvalidUserNicknameError extends Error {
    constructor(nickname:string) {
        super(`Nickname '${nickname}' is ivalid`)
    }
}