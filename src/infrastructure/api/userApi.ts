import { Request, Response } from "express"
import User from "../../domain/users/user"
import UserActions from "../../domain/users/userActions"


export interface RegisterData {
    nickname: string;
    fullName: string;
}
export interface GetFollowedData {
    nickname: string;
}
export interface LoginData {
    nickname: string;
}
export interface UserJson {
    nickname: string;
    fullName: string;
}
export interface UpdateFullNameData {
    nickname: string;
    fullName: string;
}
export interface FindOneByNicknameData {
    nickname: string;
}
export interface AddFollowData {
    followerNickname: string;
    nicknameToFollow: string;
}
export interface TweetBodyData {
    nickname: string,
    tweet: string
}
export interface GetTweetBodyData {
    nickname: string,
}


export function registerDataFromJson(json: any): RegisterData {
    return {
        ...json
    }
}

export function addFollowerDataFromJson(json: any): AddFollowData {
    return {
        ...json
    }
}
export function getFollowedDataFromJson(json: any): GetFollowedData {
    return {
        ...json
    }
}

export default class UserApi {
    private userActions: UserActions
    constructor(actions: UserActions) {
        this.userActions = actions
    }
    async tweet(req: Request, res: Response): Promise<void> {
        try {
            const body: TweetBodyData = req.body
            await this.userActions.tweet(body.nickname, body.tweet)
            res.status(204).send()
        } catch (error) {
            res.status(400).send(error.message)
        }
    }
    async login(req: Request, res: Response): Promise<void> {
        try {
            const loginData: LoginData = req.query as any
            const user = await this.userActions.login(loginData.nickname)
            res.status(200).send(user)
        } catch (error) {
            res.status(400).send(error.message)
        }
    }
    async getFollowedUsers(req: Request, res: Response): Promise<void> {
        try {
            const getFollowedData = getFollowedDataFromJson(req.query)

            const followedUsers = await this.userActions.getFollows(getFollowedData.nickname)
            res.status(200).send([...followedUsers])
        } catch (error) {
            res.status(400).send(error.message)
        }
    }
    async addFollower(req: Request, res: Response) {
        try {
            const addFollowerData = addFollowerDataFromJson(req.body)
            await this.userActions.addFollower(addFollowerData.followerNickname, addFollowerData.nicknameToFollow)
            res.status(204).send()
        } catch (error) {
            res.status(400).send(error.message)
        }
    }
    register = async (req: Request, res: Response): Promise<void> => {
        try {
            const registerData = registerDataFromJson(req.body)
            const user = await this.userActions.register(registerData.nickname, registerData.fullName)
            res.status(200).send(user)
        } catch (error) {
            res.status(400).send(error.message)
        }
    }
    updateFullName = async (req: Request, res: Response): Promise<void> => {
        try {
            const updateNameData = registerDataFromJson(req.body)
            const newName = await this.userActions.updateFullName(updateNameData.fullName, updateNameData.nickname)
            res.status(200).send(newName)
        }
        catch (error) {
            res.status(400).send(error.message)
        }
    }
    getTweets = async (req: Request, res: Response): Promise<void> => {
        try {
            const getTweetsData: GetTweetBodyData = req.query as any
            const tweets = await this.userActions.getTweets(getTweetsData.nickname)
            res.status(200).send(tweets)
        } catch (error) {
            res.status(400).send(error.message)
        }
    }
}