import User from "../../domain/users/user";
import UserNotFoundError from "../../domain/users/userNotFoundError";
import UserRepository from "../../domain/users/userRepository";

export default class InMemoryUserRepository implements UserRepository {
    users: Array<InMemoryUser> = new Array()
    constructor() {
    }
    findByNickname(nickname: string): Promise<User> {
        const user = this.users.find((user) => user.getNickname() === nickname)
        return Promise.resolve(user ? user.clone() : null)
    }
    updateUser(updatedUser: User): Promise<User> {
        const user = this.users.find((user) => user.getNickname() === updatedUser.getNickname())
        if (!user)
            throw new UserNotFoundError(updatedUser.getNickname())
        user.updateUser(updatedUser)
        return Promise.resolve(user.clone())
    }
    isNicknameAvailable(nickname: string): Promise<boolean> {
        return Promise.resolve(!this.users.some(user => user.getNickname() === nickname))
    }

    getUsers(): Promise<Array<User>> {
        return Promise.resolve(new Array(...this.users))
    }

    addNewUser(nickname: string, fullName: string): Promise<User> {
        const user = new InMemoryUser(new User(nickname, fullName))
        this.users.push(user)
        return Promise.resolve(user.clone())
    }

}
export class InMemoryUser extends User {
    constructor(user: User) {
        super(user.getNickname(), user.getFullName())
        this.updateUser(user)
    }
    public clone(): InMemoryUser {
        const user = new InMemoryUser(new User(this.nickname, this.fullName))
        user.updateUser(this)
        return user
    }
    public updateUser(updatedUser: User) {
        this.nickname = updatedUser.getNickname()
        this.fullName = updatedUser.getFullName()
        this.folledUsers = new Set<string>(updatedUser.getFollowedUsers())
        this.tweets = updatedUser.getTweets()
    }
}

