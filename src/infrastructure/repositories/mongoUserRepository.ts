import User from "../../domain/users/user";
import UserRepository from "../../domain/users/userRepository";

const url = 'mongodb://localhost:27017/twitter'
const collectionName = 'user'
export default class MongoUserRepository implements UserRepository {
    protected model = UserModel
    constructor() {
    }
    async findByNickname(nickname: string): Promise<User> {
        return userFrom(await this.model.findOne({ nickname: nickname }) as any)
    }
    async updateUser(user: User): Promise<User> {
        await this.model.update({ nickname: user.getNickname() }, { ...userDataFrom(user) })
        return user
    }
    async isNicknameAvailable(nickname: string): Promise<boolean> {
        return !(await this.findByNickname(nickname))
    }
    async getUsers(): Promise<User[]> {
        const users = await this.model.find({})
        return users.map((userData) => userFrom(userData as any))
    }
    async addNewUser(nickname: string, fullName: string): Promise<User> {
        const user: User = new User(nickname, fullName)
        const newUser = this.getBuiltModel({
            nickname: user.getNickname(), fullName: user.getFullName()
        })

        await newUser.save()
        return user
    }
    protected getBuiltModel(obj: any) {
        return new this.model(obj)
    }
}
export function userFrom(userData: UserData) {
    if (!userData)
        return null
    const user = new User(userData.nickname, userData.fullName)
    user.setFollowedUsers(new Set(userData.folledUsers))
    user.setTweets(userData.tweets)
    return user
}
export function userDataFrom(user: User) {
    if (!user)
        return null
    const userData: UserData = {
        nickname: user.getNickname(),
        fullName: user.getFullName(),
        folledUsers: [...user.getFollowedUsers()],
        tweets: user.getTweets()
    }
    return userData
}
import mongoose from 'mongoose'
var opts = { useNewUrlParser: true, connectTimeoutMS: 20000 };

mongoose.connect(url, opts).then
    (
        () => {
            console.log("DB conection on: " + url);
        },
        err => {
            console.log(url);
            console.log("ERROR:" + err);
        }
    );

var Schema = mongoose.Schema;

export interface UserData {
    tweets: Array<string>;
    nickname: string;
    fullName: string;
    folledUsers: Array<string>
}

var userSchema = new Schema<mongoose.Document<UserData>>({
    nickname: String,
    fullName: String,
    folledUsers: [String],
    tweets: [String]
});

var UserModel = mongoose.model(collectionName, userSchema);